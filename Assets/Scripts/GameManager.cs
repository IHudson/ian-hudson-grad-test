﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Game manager. Store all game variables needed.
/// </summary>
public class GameManager : MonoBehaviour
{
    //Singleton
    public static GameManager Instance { get; private set; }

    //UI
    [Header("Ball Status UI")]
    [SerializeField]
    private GameObject ballStatusUI = null;
    [SerializeField]
    private TextMeshProUGUI redBallStatusUI = null;
    [SerializeField]
    private TextMeshProUGUI yellowBallStatusUI = null;

    [Header("Game Over UI")]
    [SerializeField]
    private GameObject gameOverUI = null;
    [Space]

    //Exit door ref
    [SerializeField]
    private GameObject exitDoor;

    //Required number of ball needed to open the exit door 
    [SerializeField]
    private int NumberOfBallsNeedToOpenExit = 3;

    //Keep track of which balls have been deposited
    private int redBallsDeposited = 0;
    public int RedBallsDeposited
    {
        get
        {
            return redBallsDeposited;
        }
        set
        {
            redBallsDeposited = value;
            BallsDeposited();
        }
    }

    private int yellowBallsDeposited = 0;
    public int YellowBallsDeposited
    {
        get
        {
            return yellowBallsDeposited;
        }
        set
        {
            yellowBallsDeposited = value;
            BallsDeposited();
        }
    }

    private void Awake()
    {
        //Setup instance
        if(Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    /// <summary>
    /// Check if enough balls have been deposited
    /// </summary>
    private void BallsDeposited()
    {
        if (RedBallsDeposited >= NumberOfBallsNeedToOpenExit && YellowBallsDeposited >= NumberOfBallsNeedToOpenExit)
        {
            //open exit
            exitDoor.SetActive(false);
        }
    }

    /// <summary>
    /// Enable the ball status UI
    /// </summary>
    public void BallsStatusUI()
    {
        //display UI with ball count
        redBallStatusUI.text = "Red: " + RedBallsDeposited + "/" + NumberOfBallsNeedToOpenExit;
        yellowBallStatusUI.text = "Yellow: " + YellowBallsDeposited + "/" + NumberOfBallsNeedToOpenExit;
        StartCoroutine(BallStatusUICoro());
    }

    /// <summary>
    /// Enable the ball status UI then disable it after a number of seconds
    /// </summary>
    private IEnumerator BallStatusUICoro()
    {
        ballStatusUI.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        ballStatusUI.SetActive(false);
    }

    /// <summary>
    /// The player has lost all of their lives. 
    /// </summary>
    public void GameOver()
    {
        gameOverUI.SetActive(true);

        //Pause the game
        Time.timeScale = 0;
    }
}
