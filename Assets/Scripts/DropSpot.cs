﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Drop spot. Check if a ball has been dropped.
/// </summary>
public class DropSpot : MonoBehaviour
{
    //Define the two different tube colours
    enum TubeColour
    {
        Red,
        Yellow
    }

    [SerializeField]
    private TubeColour tubeColour;

    private void OnTriggerEnter(Collider other)
    {
        //Increment the player ball count only if a red ball has been placed in a red tube or 
        //yellow ball has been placed in a yellow tube
        if (other.tag == "RedBall" && tubeColour == TubeColour.Red)
        {
            GameManager.Instance.RedBallsDeposited++;
            other.gameObject.SetActive(false);
        }
        else if(other.tag == "YellowBall" && tubeColour == TubeColour.Yellow)
        {
            GameManager.Instance.YellowBallsDeposited++;
            other.gameObject.SetActive(false);
        }
    }
}
