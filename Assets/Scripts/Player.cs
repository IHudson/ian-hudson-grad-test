﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player class. 
/// </summary>
public class Player : MonoBehaviour
{
    [SerializeField]
    private int playerLives = 3;

    [SerializeField]
    private Transform startPosition;

    [SerializeField]
    private AudioSource beepSound;
    private float beepSoundMaxDistance = 10.0f;
    private bool playBeep = true;

    [SerializeField]
    private GameObject enemy1;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(BeepSound());
    }

    // Update is called once per frame
    void Update()
    {
    }

    private IEnumerator BeepSound()
    {
        float step = 0.0f;

        while (playBeep)
        {
            step += Time.deltaTime;

            float distance = Vector3.Distance(enemy1.transform.position, transform.position);

            float threshhold = distance * 0.1f;
            if (step > Mathf.Clamp(threshhold, 0.5f, beepSoundMaxDistance))
            {
                step = 0.0f;
                beepSound.Play();
            }

            yield return null;
        }
    }

    /// <summary>
    /// Reduce the number of lives. Default value is 1
    /// </summary>
    public void ReduceLives(int a_reduceBy = 1)
    {
        playerLives -= a_reduceBy;

        if(playerLives == 0)
        {
            GameManager.Instance.GameOver();
        }
        else
        {
            transform.position = startPosition.position;
        }
        //After the player game object has been moved re enable the game object
        //and start the beep sound coroutine again
        gameObject.SetActive(true);
        StartCoroutine(BeepSound());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            //Stop coroutine and disable the player game object
            StopCoroutine(BeepSound());
            gameObject.SetActive(false);
            ReduceLives();
        }
    }
}
