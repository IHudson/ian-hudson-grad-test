﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Slow enemy agent
/// </summary>
public class SlowEnemyAgent : Agent
{
    public SlowEnemyAgent() : base()
    {
        //Add all states to the state machine
        stateMachine.AddState("ChaseState", new ChaseState(false, 0.0f));
        //Change current state
        stateMachine.ChangeState("ChaseState");
    }
}
