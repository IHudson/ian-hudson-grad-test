﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Fast moving enemt agent.
/// </summary>
public class FastEnemyAgent : Agent
{
    //Three different paths for the agent to take
    [SerializeField]
    private Transform[] waypointsPath1;
    [SerializeField]
    private Transform[] waypointsPath2;
    [SerializeField]
    private Transform[] waypointsPath3;

    public FastEnemyAgent() : base()
    {

    }

    protected override void Start()
    {
        //Add all states to the state machine
        stateMachine.AddState("PatrolState", new PatrolState(waypointsPath1, waypointsPath2, waypointsPath3));
        stateMachine.AddState("ChaseState", new ChaseState(true, 7.0f));
        //Change current state
        stateMachine.ChangeState("PatrolState");

        navMeshAgent = GetComponent<NavMeshAgent>();
    }
}
