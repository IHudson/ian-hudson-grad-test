﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// State machine for AI. Push and pop available states onto the stack.
/// </summary>
public class StateMachine
{
    //Dictionary for all possible
    private Dictionary<string, State> allStates;

    // Stack for all active states
    private Stack stateStack;

    //Owner agent
    Agent owner;

    public StateMachine(Agent a_owner)
    {
        owner = a_owner;
        stateStack = new Stack();
        allStates = new Dictionary<string, State>();
    }

    public void Update()
    {
        State currentState = GetCurrentState();
        if(currentState != null)
        {
            currentState.Update(this);
        }
    }

    /// <summary>
    /// Add a new possible state to the state machine
    /// </summary>
    public void AddState(string a_key, State a_state)
    {
        allStates.Add(a_key, a_state);
    }

    /// <summary>
    /// Change the current state to a new state
    /// </summary>
    public void ChangeState(string a_state)
    {
        //Check if we have the state available 
        if (allStates.ContainsKey(a_state))
        {
            //Check if stack has the state in it
            if (stateStack.Contains(allStates[a_state]))
            {
                //If the stack does then pop states until we reach the new state
                while (GetCurrentState() != allStates[a_state])
                {
                    GetCurrentState().Exit(this);
                    Pop();
                }
            }
            else
            {
                //Stack does not have new state in it. Push the new state onto the stack
                Push(allStates[a_state]);
            }
        }
        else
        {
            //allStates does not contain wanted state. Error message
            Debug.LogError("State machine on " + owner + ", does not have " + a_state + " state available to use.");
        }
    }

    /// <summary>
    /// Push a new state onto the stack
    /// </summary>
    /// <param name="a_state"></param>
    private void Push(State a_state)
    {
        if(GetCurrentState() != a_state)
        {
            stateStack.Push(a_state);
            GetCurrentState().Enter(this);
        }
    }

    /// <summary>
    /// Pop the top state off the stack
    /// </summary>
    private void Pop()
    {
        stateStack.Pop();
    }

    /// <summary>
    /// Get the current state
    /// </summary>
    /// <returns></returns>
    private State GetCurrentState()
    {
        if(stateStack.Count > 0)
        {
            return (State)stateStack.Peek();
        }

        return null;
    }

    /// <summary>
    /// Get the owner agent for this state machine
    /// </summary>
    /// <returns></returns>
    public Agent GetOwner()
    {
        return owner;
    }
}
