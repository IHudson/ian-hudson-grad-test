﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Base agent class. Recomended to be drevided from.
/// </summary>
public class Agent : MonoBehaviour
{
    //State machine for this agent 
    protected StateMachine stateMachine;

    [SerializeField]
    protected float movementSpeed;
    public float MovementSpeed { get { return movementSpeed; } }

    //Ref to the nav mesh agent 
    protected NavMeshAgent navMeshAgent;
    public NavMeshAgent NavMeshAgent { get { return navMeshAgent; } }

    //Player ref 
    [SerializeField]
    private GameObject player;
    public GameObject Player { get { return player; } }

    public Agent()
    {
        stateMachine = new StateMachine(this);
    }

    protected virtual void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    protected virtual void Update()
    {
        stateMachine.Update();
    }
}
