﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Chase state. Chase the player.
/// </summary>
public class ChaseState : State
{
    //Min distance needed to start chasing the player
    private float distanceToChase;
    //Is the distance need to chase the player
    bool distanceNeeded;

    public ChaseState(bool a_distanceNeeded, float a_distanceToChase)
    {
        distanceToChase = a_distanceToChase;
        distanceNeeded = a_distanceNeeded;
    }

    public override void Enter(StateMachine a_owner)
    {
    }

    public override void Update(StateMachine a_owner)
    {
        //Check if agent has to be within a distance to chase. If true check if agent is within the distance to chase
        if (!distanceNeeded || Vector3.Distance(a_owner.GetOwner().transform.position, a_owner.GetOwner().Player.transform.position) <= distanceToChase)
        {
            //Agent is close to the player. Chase the player 
            a_owner.GetOwner().NavMeshAgent.SetDestination(a_owner.GetOwner().Player.transform.position);
            a_owner.GetOwner().NavMeshAgent.speed = a_owner.GetOwner().MovementSpeed;
        }
        else
        {
            //Return the to the patrol state. Agent is too far away from the player
            a_owner.ChangeState("PatrolState");
        }
    }

    public override void Exit(StateMachine a_owner)
    {
    }
}
