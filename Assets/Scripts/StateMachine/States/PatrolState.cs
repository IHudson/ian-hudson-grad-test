﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Patrol state. Move between waypoints
/// </summary>
public class PatrolState : State
{
    //All waypoint paths
    private List<Transform[]> waypoints;



    //time elapsed since last path change
    private float pathTimer;
    private float pathTimerMax = 10;

    //current path and way point on path
    private int currentIndexOnPath = -1;
    private int currentPath = 0;

    public PatrolState(Transform[] a_waypoints, Transform[] a_waypoints1, Transform[] a_waypoints2)
    {
        waypoints = new List<Transform[]>();
        waypoints.Add(a_waypoints);
        waypoints.Add(a_waypoints1);
        waypoints.Add(a_waypoints2);
    }

    public override void Enter(StateMachine a_owner)
    {
    }

    public override void Update(StateMachine a_owner)
    {
        //If the agent is within a distance to the player. Change state
        if(Vector3.Distance(a_owner.GetOwner().transform.position, a_owner.GetOwner().Player.transform.position) < 5)
        {
            a_owner.ChangeState("ChaseState");
        }
        else
        {
            pathTimer += Time.deltaTime;
            if(pathTimer >= pathTimerMax)
            {
                //loop the currentPath from 0 to waypoints.Count
                currentPath = (currentPath + 1) % waypoints.Count;
                pathTimer = 0.0f;
            }

            EvaluatePath(a_owner, waypoints[currentPath], currentIndexOnPath);

        }
    }

    /// <summary>
    /// Evaluate the current path
    /// </summary>
    private void EvaluatePath(StateMachine a_owner, Transform[] a_waypoints, int a_index)
    {
        if (a_owner.GetOwner().NavMeshAgent.remainingDistance < 0.5f)
        {
            currentIndexOnPath++;
            if (currentIndexOnPath >= a_waypoints.Length)
            {
                //loop the currentPath from 0 to a_waypoints.Length
                currentIndexOnPath = (currentIndexOnPath + 1) % a_waypoints.Length;
            }
            else
            {
                a_owner.GetOwner().NavMeshAgent.SetDestination(a_waypoints[currentIndexOnPath].position);
            }
        }
    }

    public override void Exit(StateMachine a_owner)
    {
    }
}