﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base state class. Can not be implemented. Must be inherited from
/// </summary>
public abstract class State
{
    StateMachine stateMachineOwner;
    public virtual void Enter(StateMachine a_owner) { stateMachineOwner = a_owner; }
    public virtual void Update(StateMachine a_owner) { }
    public virtual void Exit(StateMachine a_owner) { }
}