﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Pickup objects. Used to pickup balls throughout the level.
/// </summary>
public class Pickup : MonoBehaviour
{
    //Define which tags can be interactive with
    [SerializeField]
    private InteractiveTags[] interativeTags;

    [SerializeField]
    private float pickupDistance = 3.5f;

    //UI
    [SerializeField]
    private GameObject pickupUI;
    [SerializeField]
    private TextMeshProUGUI displayText;

    //Object currently being held
    [SerializeField]
    private GameObject objectPickedup;

    // Update is called once per frame
    void Update()
    {

        RaycastHit hit;
        Ray cameraRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        bool hitInterativeTag = false;
        if(Physics.Raycast(cameraRay, out hit, pickupDistance))
        {
            for (int i = 0; i < interativeTags.Length; i++)
            {
                //Check if the object hit has the same tag
                if(hit.collider.tag == interativeTags[i].Tag)
                {
                    if(Input.GetKeyDown(KeyCode.E))
                    {
                        //If we are holding an object. Drop it. Then pick up the new object
                        if(objectPickedup)
                        {
                            DropObject();
                        }

                        objectPickedup = hit.collider.gameObject;
                        objectPickedup.GetComponent<Rigidbody>().useGravity = false;
                        objectPickedup.GetComponent<Rigidbody>().isKinematic = true;
                    }

                    pickupUI.SetActive(true);
                    displayText.color = interativeTags[i].Colour;
                    hitInterativeTag = true;

                    //Object picked up return
                    return;
                }
            }
        }

        if (objectPickedup)
        {
            //Set position of object picked up
            objectPickedup.transform.position = transform.position + (transform.forward * 1f);

            //Drop object
            if (Input.GetKeyDown(KeyCode.E))
            {
                DropObject();
            }
        }

        //If we are not looking at a tagged object which can be interacted with
        //or we have an object picked up
        if(!hitInterativeTag || objectPickedup)
        {
            //Disable the pick UI
            pickupUI.SetActive(false);
        }
    }

    /// <summary>
    /// Drop an object
    /// </summary>
    private void DropObject()
    {
        objectPickedup.GetComponent<Rigidbody>().useGravity = true;
        objectPickedup.GetComponent<Rigidbody>().isKinematic = false;
        objectPickedup = null;
    }
}

/// <summary>
/// Define tag with a colour
/// </summary>
[System.Serializable]
struct InteractiveTags
{
    public string Tag;
    public Color Colour;
}
