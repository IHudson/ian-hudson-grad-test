﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Door script. Check if the door should open.
/// </summary>
public class Door : MonoBehaviour
{
    //Tags for the door to open for 
    [SerializeField]
    private string[] openOnTags;

    //Game object for the door
    [SerializeField]
    private GameObject doorGO;

    //Is this the exit door
    [SerializeField]
    private bool isExitDoor = false;

    [SerializeField]
    private Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (isExitDoor)
        {
            //display ball count 
            GameManager.Instance.BallsStatusUI();
        }
        else
        {
            //Check if collider has the same tag as openOnTags element
            for (int i = 0; i < openOnTags.Length; i++)
            {
                if (other.tag == openOnTags[i])
                {
                    //doorGO.SetActive(false);
                    animator.SetBool("Open", true);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(!isExitDoor)
        {
            //Check if collider has the same tag as openOnTags element
            for (int i = 0; i < openOnTags.Length; i++)
            {
                if (other.tag == openOnTags[i])
                {
                    //doorGO.SetActive(true);
                    animator.SetBool("Open", false);
                }
            }
        }
    }
}
