﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawn objects within the level
/// </summary>
public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] ballObjectsToSpawn;
    [SerializeField]
    private GameObject[] tubeObjectsToSpawn;

    [SerializeField]
    private Collider[] spawnLocatoions;

    [Header("Tube Variable")]
    [Tooltip("Spawn the number of tubes for each colour.")]
    [SerializeField]
    private int NumberOfTubesToSpawn = 5;


    [Header("Ball Variable")]
    [Tooltip("Spawn the number of balls for each colour.")]
    [SerializeField]
    private int NumberOfBallsToSpawn = 5;

    [SerializeField]
    private float spawnRadiusSize = 1;

    // Start is called before the first frame update
    void Start()
    {
        SpawnTubeObjects();
        SpawBallObjects();
        for (int i = 0; i < spawnLocatoions.Length; i++)
        {
            spawnLocatoions[i].enabled = false;
        }
    }

    /// <summary>
    /// Spawn all tube objects
    /// </summary>
    private void SpawnTubeObjects()
    {

        //Loop though all tube objects
        for (int i = 0; i < tubeObjectsToSpawn.Length; i++)
        {
            //Store if a room has a coloured tube spawned in it
            List<int> roomCount = new List<int>();

            //Spawn attempts
            int attempets = 0;
            
            //Spawn need number of tube objects
            for (int j = 0; j < NumberOfTubesToSpawn; j++)
            {
                int randomRoom = Random.Range(0, spawnLocatoions.Length);
                //If the room number is in roomCount List then a tube has been spawned there
                //decrement j and continue. This ensures we have the correct number of spawns
                if (roomCount.Contains(randomRoom))
                {
                    j--;
                    continue;
                }
                else
                {
                    roomCount.Add(randomRoom);
                }

                Vector3 spawnPos = GetPositionInBounds(spawnLocatoions[randomRoom].bounds);
                spawnPos.y = 0;

                //Before spawn check no other object is overlapping
                if(!CheckSpawnLoc(spawnPos))
                {
                    attempets++;
                    //if attempts is less than 25 then keep trying to spawn this object
                    //otherwise discard and move on
                    if(attempets < 25)
                    {
                        j--;
                    }
                    continue;
                }

                GameObject go = Instantiate(tubeObjectsToSpawn[i]);
                go.transform.position = spawnPos;
            }
        }
    }

    private bool CheckSpawnLoc(Vector3 a_loc)
    {
        Collider[] colliders = Physics.OverlapSphere(a_loc, spawnRadiusSize);

        for (int i = 0; i < colliders.Length; i++)
        {
            if(colliders[i].tag == "Player" || colliders[i].tag == "RedBall" || colliders[i].tag == "YellowBall" ||
                colliders[i].tag == "RedDropSpot" || colliders[i].tag == "YellowDropSpot")
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Spawn all ball objects 
    /// </summary>
    private void SpawBallObjects()
    {
        //Store the rooms with balls spawned in them
        List<int> roomCount = new List<int>();

        //Loop though all ball objects
        for (int i = 0; i < ballObjectsToSpawn.Length; i++)
        {
            //Spawn need number of ball objects
            for (int j = 0; j < NumberOfBallsToSpawn; j++)
            {
                int randomRoom = Random.Range(0, spawnLocatoions.Length);
                //If the room number is in roomCount List then a ball has been spawned there
                //decrement j and continue. This ensures we have the correct number of spawns
                if (roomCount.Contains(randomRoom))
                {
                    j--;
                    continue;
                }
                else
                {
                    roomCount.Add(randomRoom);
                }

                Vector3 spawnPos = GetPositionInBounds(spawnLocatoions[randomRoom].bounds);

                GameObject go = Instantiate(ballObjectsToSpawn[i]);
                go.transform.position = spawnPos;
            }
        }
    }

    /// <summary>
    /// Return a random point in bounds
    /// </summary>
    /// <param name="a_bounds"></param>
    /// <returns></returns>
    private Vector3 GetPositionInBounds(Bounds a_bounds)
    {
        return new Vector3(
            Random.Range(a_bounds.min.x, a_bounds.max.x),
            Random.Range(a_bounds.min.y, a_bounds.max.y),
            Random.Range(a_bounds.min.z, a_bounds.max.z));
    }
}
